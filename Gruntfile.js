module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    watch: {
      // if any .less file changes in directory "public/css/" run the "less"-task.
      files: ["src/static/css/less/**/*.less", "src/static/css/less/**/**/*.less"],
      tasks: ["less"]
    },
    less: {
      options: {
        // Specifies directories to scan for @import directives when parsing.
        // Default value is the directory of the source, which is probably what you want.
        // paths: ["css/less", "bower_components/bootstrap/less"]
      },
      files: {
        // compilation.css  :  source.less
        src: "src/static/css/less/style.less",
        dest: "src/static/css/app.css"
      }
    }
  });

  // grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('styles', ['less'])

};
