angular.module('myApp').
  directive('ltEditable', ['$filter', function ($filter) {
    return {
      restrict : 'A',
      require: '?ngModel',
      scope: {
        maxWidth: '@',
        maxLength: '@',
        autoSelect: '='
      },
      link: function ($scope, el, attrs, ngModel) {
        if(!ngModel) return;

        var container = el,
            children = el.children(),
            frontEl = $(children[0]),
            hiddenInput = $(children[1]),
            preIcon = el.prev(),
            originalText,
            editIcon;

        container.append('<i class="glyphicon glyphicon-pencil"></i>');
        editIcon = container.children('i');

        hiddenInput.addClass('editable-input');
        setStyle();

        var clicked = false;
        frontEl.on('click', function () {
          clicked = true;
          editIcon.css('visibility', 'hidden');
          frontEl.css("display", "none");
          hiddenInput.css("display", "block");
          originalText = ngModel.$viewValue;
          hiddenInput.select();
        });

        container.on('mouseover', function () {
          if (!clicked) {
            editIcon.css({
              'left': (frontEl.width() + 7) + 'px',
              'visibility': 'visible'
            });
          }
        });

        container.on('mouseout', function () {
          editIcon.css('visibility', 'hidden');
        });

        hiddenInput.on('blur', function () {
          clicked = false;
          hiddenInput.val(checkInput());
          frontEl.text(hiddenInput.val());
          $scope.$apply(read);
          hiddenInput.css("display", "none");
          frontEl.css("display", "block");
        });

        // In order, keycodes for:
        // backspace, delete, enter and escape, left/right arrows
        var authorizedKeys = [8, 46, 13, 27, 37, 39],
            maxLengthNum;

        hiddenInput.on('keydown',function (event) {
          if (event.which === 27) {
            inputEscape();
            return;
          }

          if (event.which === 13) {
            inputEnter();
            return;
          }

          if (maxLengthNum && hiddenInput.val().length >= maxLengthNum) {

            var isAuthorized = function (element) {
              return event.which === element;
            };

            if (authorizedKeys.some(isAuthorized)) {
              return;
            } else {
              event.preventDefault();
            }
          }
        });

        $scope.$watch('maxLength', function (newMaxLength) {
          if (newMaxLength) {
            maxLengthNum = parseInt(newMaxLength);
          }
        });

        $scope.$watch('autoSelect', function (newVal) {
          if (newVal) {
            frontEl.click();
            $scope.autoSelect = false;
          }
        });

        function setStyle() {
          var fontSize,
              divHeight,
              marginTop = 0,
              marginBottom = 0,
              inputTop = 0,
              frontElSize,
              frontElName = frontEl[0].localName;

          switch (frontElName) {
            case 'h1':
              frontElSize = 39;
              fontSize = 36;
              inputTop = -7;
              break;
            case 'h2':
              frontElSize = 33;
              fontSize = 30;
              inputTop = -5;
              break;
            case 'h3':
              frontElSize = 26;
              fontSize = 24;
              inputTop = -5;
              break;
            case 'h4':
              frontElSize = 19;
              fontSize = 18;
              inputTop = -4;
              break;
            case 'h5':
              frontElSize = 15;
              fontSize = 14;
              inputTop = -3;
              break;
          }


          if (frontElName === 'h1' || frontElName === 'h2' || frontElName === 'h3') {
            marginTop = 20;
            marginBottom = 10;
          } else if (frontElName === 'h4' || frontElName === 'h5') {
            marginTop = 10;
            marginBottom = 10;
          }

          // The removing of margin cannot be done otherwise because the css is inlined,
          // but the editable class will often be used with page-header so it is not too custom.
          if (container.closest('.page-header').length) {
            marginTop = 0;
          }

          frontEl.css('height', fontSize + 6);

          container.css({
            'min-width': parseInt($scope.maxWidth) + '%',
            'height': frontElSize + 'px',
            'margin-top': marginTop + 'px',
            'margin-bottom': marginBottom + 'px'
          });

          hiddenInput.css({
            'font-size': fontSize + 'px',
            'top': inputTop + 'px',
            'width': '90%'
          });

          frontEl.css('max-width', '90%');

          editIcon.css('font-size', (fontSize * 2 / 3) + 'px');

          if (preIcon) {
            preIcon.css({
              'font-size': fontSize,
              'top': 4 + 'px'
            });
          }
        }

        function inputEnter() {
          hiddenInput.blur();
        }

        function inputEscape() {
          hiddenInput.val(originalText);
          hiddenInput.blur();
        }

        function read() {
          ngModel.$setViewValue(frontEl.text());
        }

        ngModel.$render = function() {
          frontEl.text(ngModel.$viewValue);
          hiddenInput.val(ngModel.$viewValue);
        };

        function checkInput() {
          var strippedContent = $filter('ltStripWhiteSpaces')(hiddenInput.val());
          strippedContent = $filter('limitTo')(strippedContent, maxLengthNum);

          return strippedContent ? strippedContent : originalText;
        }
      }
    };
  }
]);
