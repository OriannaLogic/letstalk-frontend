angular.module('myApp')
  .directive('ltLocation', function () {
    return {
      restrict: 'E',
      scope: {
        registered: '=',
        online: '=',
        writing: '='
      },
      templateUrl: 'src/js/directives/ui/location/location.html',
      link: function (scope, el, attrs) {
      }
    };
  });
