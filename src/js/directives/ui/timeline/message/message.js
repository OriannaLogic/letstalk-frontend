angular.module('myApp')
  .directive('ltMessage', function () {
    return {
      restrict: 'E',
      scope: {
        message: '='
      },
      templateUrl: 'src/js/directives/ui/timeline/message/message.html',
      link: function ($scope, el, attrs) {
        $scope.hiddenContent = ''
        $scope.shownContent = ''
        $scope.hide = true;
        $scope.extendableTitle = "Show more..."

        $scope.$watch('message', function () {
          messageContent = $scope.message.content

          if (messageContent.length) {
            if (messageContent.length > 700) {
              $scope.shownContent = messageContent.substring(0, 701)
              $scope.hiddenContent = messageContent.substring(701, messageContent.length)
            } else {
              $scope.shownContent = messageContent
            }
          }
        });

        $scope.showHidden = function () {
          if ($scope.hide == true) {
            $scope.shownContent = $scope.message.content;
            $scope.extendableTitle = "Hide"
            $scope.hide = false
          } else {
            $scope.shownContent = $scope.message.content.substring(0, 701)
            $scope.extendableTitle = "Show more..."
            $scope.hide = true;
          }
        }
      }
    };
  });
