angular.module('myApp')
  .directive('ltTimeline', function () {
    return {
      restrict: 'E',
      scope: {
        messages:'='
      },
      templateUrl: 'src/js/directives/ui/timeline/timeline.html',
      link: function (scope, el, attrs) {
      }
    };
  });
