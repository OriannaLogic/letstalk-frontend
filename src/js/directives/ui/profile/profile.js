angular.module('myApp')
  .directive('ltProfile', function () {
    return {
      restrict: 'E',
      scope: {
        profile: '=',
        location: '='
      },
      templateUrl: 'src/js/directives/ui/profile/profile.html',
      link: function (scope, el, attrs) {
      }
    };
  });
