angular.module('myApp')
  .directive('ltExpress', function () {
    return {
      restrict: 'E',
      scope: {
        messageContent: '=',
        action: '&'
      },
      templateUrl: 'src/js/directives/ui/express/express.html',
      link: function (scope, el, attrs) {
      }
    };
  });
