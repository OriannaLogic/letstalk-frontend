angular.module('myApp')
 .directive('ltExtendable', function () {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        extensionTitle:'=',
        position: '@'
      },
      templateUrl: 'src/js/directives/ui/extendable/extendable.html',
      link: function ($scope, el, attr) {
        $scope.extended = false;
      }
    };
});
