angular.module('myApp')
  .directive('ltPeople', function () {
    return {
      restrict: 'E',
      scope: {
        location:'='
      },
      templateUrl: 'src/js/directives/ui/people/people.html',
      link: function (scope, el, attrs) {
      }
    };
  });
