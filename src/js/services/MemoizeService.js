angular.module('myApp')
  .service('MemoizeService', function () {

    return {
      memoizeWithArgs : function (fn) {
        return function () {
          var args = Array.prototype.slice.call(arguments),
              hash = "",
              i = args.length,
              currentArg = null;

          if (!fn.memoize) {
            fn.memoize = {};
          }

          while (i--) {
            currentArg = args[i];
            hash += (currentArg === Object(currentArg)) ?
              JSON.stringify(currentArg) : currentArg;
          }

          return (hash in fn.memoize) ? fn.memoize[hash] : fn.memoize[hash] = fn.apply(this, args);
        };
      },

      simpleMemoize: function (fn) {
        return function () {
          var args = Array.prototype.slice.call(arguments);
          return fn.memoize ? fn.memoize : fn.memoize = fn.apply(this, args);
        };
      }
    };
  });