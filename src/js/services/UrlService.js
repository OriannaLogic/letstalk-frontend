angular.module('myApp')
  .service('UrlService', function () {
    var APP_DOMAIN = 'http://localhost:8080';
    var API_DOMAIN = 'http://www.espo-server.com:8002/';

    var urlParsingNode = document.createElement('a');

    return {
      parse: function (url) {
        var upn = urlParsingNode;

        upn.href = url;

        return {
          host: upn.host,
          hostname: upn.hostname,
          pathname: upn.pathname,
          search: upn.search,
          path: upn.pathname + upn.search,
          href: upn.href
        };
      },

      getBaseDomain: function (url) {
        if (!url) {
          url = window.location.href;
        }

        return this.parse(url).hostname.split('.').splice(1).join('.');
      },

      getHomeUrl: function () {
        return APP_DOMAIN;
      },

      getApiUrl: function () {
        return API_DOMAIN;
      }
    };
  }
);
