angular.module('myApp')
  .service('UserService', ['$rootScope', 'Restangular', 'UrlService', 'MemoizeService', '$q',
    function ($rootScope, Restangular, UrlService, MemoizeService, $q) {
      return {
        current: {},

        getCurrent: function () {
          if (this.current && this.current.id) {
            var deferred = $q.defer();

            deferred.resolve(this.current);

            return deferred.promise;
          } else {
            return Restangular.one('users', 'me').get();
          }
        },

        getRandom: function (userId) {
          return Restangular.all('users').one('random', userId).get();
        },

        appendMessage: function (newMessage) {
          return this.getCurrent().then(function (user) {
            return user.save({ message: newMessage });
          });
        },

        updatePseudo: function (newPseudo) {
          return this.getCurrent().then(function (user){
            return user.save({ pseudo: newPseudo });
          });
        },

        login: function (username, password) {
          return Restangular.all('users').login({ username: username, password: password });
        },

        logout: function (username, password) {
          return Restangular.all('users').logout();
        },

        signup: function (username, password) {
          return Restangular.all('users').signup({ username: username, password: password });
        },

        setCurrent: function (user) {
          this.current = user;
        }
      };
  }]);
