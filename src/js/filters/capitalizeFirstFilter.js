angular.module('myApp')
  .filter('ltCapitalizeFirst', ['$filter', function ($filter) {
    return function (str) {
      if (typeof str === 'string') {
        return $filter('uppercase')(str.charAt(0)) + str.slice(1);
      }
    };
  }
]);