angular.module('myApp')
  .filter('ltStripWhiteSpaces', ['$filter', function ($filter) {
    return function (str) {
      var stripped = str;
      stripped = stripped.replace(/\s+/g, ' ');
      stripped = stripped.replace(/^(\s+)|(\s+)$/g, '');
      return stripped;
    };
  }
]);