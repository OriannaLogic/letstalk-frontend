angular.module('myApp')
  .filter('ltHumanize', ['$filter', function ($filter) {
    return function (str) {
      if (typeof str === 'string') {
        if (str.indexOf('_') > -1) {
          var strArray = str.split('_').map(function (str) {
            return $filter('lowercase')(str);
          });

          return $filter('ltCapitalizeFirst')(strArray[0]) + ' ' + strArray[1];
        } else {
          return $filter('ltCapitalizeFirst')($filter('lowercase')(str));
        }
      }
    };
  }
]);