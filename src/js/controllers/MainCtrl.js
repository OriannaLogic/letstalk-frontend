myApp.controller('MainCtrl', ['$rootScope', '$scope', 'UserService', 'user', function ($rootScope, $scope, UserService, user) {
  $scope.registered = '80'
  $scope.online = '17'
  $scope.writing = '5'
  $scope.location = "Partout"

  $rootScope.watchedUser = $rootScope.user;

  $scope.getRandomUser = function () {
    var u = $rootScope.watchedUser;

    UserService.getRandom(u.id).then(function (user) {
      $rootScope.watchedUser = user;
    });
  }
}]);