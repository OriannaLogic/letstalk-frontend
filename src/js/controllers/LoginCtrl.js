myApp.controller('LoginCtrl',
  ['$scope', '$rootScope', 'UserService', 'UrlService', function ($scope, $rootScope, UserService, UrlService) {
    var stateName = $rootScope.$state.current.name;

    if (stateName.indexOf('login') > -1) {
      $scope.loginError = false;

      $scope.goToMain = function () {
        UserService.login($scope.username, $scope.password).then(function (user) {
          UserService.setCurrent(user);
          $rootScope.$state.go("main.timeline");
        }, function (err) {
          $scope.loginError = true;
        });
      };
    }

    if (stateName.indexOf('signup') > -1) {
      $scope.signupError = false;

      $scope.goToMain = function () {
        UserService.signup($scope.username, $scope.password).then(function (user) {
          UserService.setCurrent(user);
          $rootScope.$state.go("main.timeline");
        }, function (err) {
          $scope.signupError = true;
        });
      };
    }

    $scope.setUserToCurrent = function () {
      $rootScope.watchedUser = $rootScope.user;
    };

    if (stateName.indexOf('main') > -1) {
      $scope.logout = function () {
        UserService.logout().then(function () {
          UserService.setCurrent(null);
          $rootScope.$state.go('login');
        }, function (err) {
          console.log(err);
        });
      };

      $scope.pseudo = $rootScope.user.pseudo;

      $scope.$watch('pseudo', function (newPseudo, oldPseudo) {
        if (newPseudo && newPseudo != $rootScope.user.pseudo) {
          UserService.updatePseudo(newPseudo).then(function() {
            $rootScope.user.pseudo = newPseudo;
          }, function () {
            $scope.pseudo = $rootScope.user.pseudo;
          });
        }
      });
    }
}]);