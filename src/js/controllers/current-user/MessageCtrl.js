myApp.controller('MessageCtrl', ['$rootScope', '$scope', '$state', 'UserService',
  function ($rootScope, $scope, $state, UserService) {
    $scope.addMessage = function (title, content) {
      message = {
        content: content,
        title: title
      };

      UserService.appendMessage(message).then(function (newMessage) {
        console.log(newMessage)
        $scope.messageContent = "";
        $scope.messageTitle = "";
        $rootScope.user.messages.push(newMessage);

        $state.go("^.timeline");
      });
    };
  }
]);