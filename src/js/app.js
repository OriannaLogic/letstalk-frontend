  var myApp = angular.module('myApp', ['ui.router', 'restangular']);

myApp


  .config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider.setBaseUrl('http://www.espo-server.com:8002/');

    RestangularProvider.addElementTransformer('users', true, function(user) {
      user.addRestangularMethod('login', 'post', 'login');
      user.addRestangularMethod('logout', 'get', 'logout');
      user.addRestangularMethod('signup', 'post', 'signup');
      return user;
    });

    RestangularProvider.setDefaultHttpFields({
      'withCredentials': true
    });
  }])
  .config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
  }])
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/landing.html");

    $stateProvider
      .state('landing', {
        url: "/landing.html",
        views: {
          "main": {
            controller: "LandingCtrl",
            templateUrl: "src/templates/landing.html",
          }
        }
      })
      .state('login', {
        url: "/login.html",
        views: {
          "main": {
            controller: "LoginCtrl",
            templateUrl: "src/templates/login.html",
          }
        }
      })
      .state('signup', {
        url: "/signup.html",
        views: {
          "main": {
            controller: "LoginCtrl",
            templateUrl: "src/templates/login.html",
          }
        }
      })
      .state('main', {
        url: "/main.html",
        abstract: true,
        views: {
          'main': {
            templateUrl: "src/templates/main.html",
            controller: "MainCtrl"
          },
          'navbar': {
            templateUrl: "src/templates/navbar.html",
            controller: "LoginCtrl"
          },
        },
        resolve: {
            user: ['UserService', '$rootScope', '$q', '$state', function (UserService, $rootScope, $q, $state) {
              return UserService.getCurrent()
                .then(function (user) {
                  $rootScope.user = user;
                  return user;
                }, function (err) {
                  $state.go("landing");
                });
            }],
        }
      })
      .state('main.timeline', {
        url: "/main/timeline.html",
        template: '<lt-timeline messages="watchedUser.messages"></lt-timeline>'
      })
      .state('main.express', {
        url: "/main/express-yourself.html",
        template: '<lt-express message-content="newMessageContent" action="addMessage(title, content)"></lt-express>',
        controller: "MessageCtrl"
      });
  }])
  .run(['$rootScope', '$state', function ($rootScope, $state) {
    $rootScope.$state = $state;

    $rootScope.$on('stateChangeError', function (e, ts, tp, fs, fp) {
      console.log(e, ts, tp, ts, fp);
      $state.transitionTo("landing");
    });
  }]);